require 'rubygems'
require_relative 'spec_helper'

RSpec.describe TxCatcher::LOGGER do

  describe "logging to logfiles" do

    it "writes info messages to txcatcher.log" do
      TxCatcher::LOGGER.report "example info message", :info
      expect(File.read(LOGFILE)).to include("example info message")
    end

    it "writes errors into a separate error.log file" do
      TxCatcher::LOGGER.report "example error message", :error
      expect(File.read(ERRFILE)).to include("example error message")
    end

  end

  describe "sending to Sentry" do

    it "logs messages to Sentry" do
      expect(Raven).to receive(:capture_message).once
      TxCatcher::LOGGER.report "example info message", :info
      TxCatcher::LOGGER.report "example error message", :error
    end

    it "logs errors to Sentry" do
      expect(Raven).to receive(:capture_exception).once
      TxCatcher::LOGGER.report StandardError.new, :error
    end

  end

  it "logs to stdout" do
    expect($stdout).to receive(:print).exactly(3).times
    TxCatcher::LOGGER.report "example info message", :info
    TxCatcher::LOGGER.report "example error message", :error
  end

  it "doesn't log an message if its loglevel is below current threshold" do
    TxCatcher::Config["logger"]["stdout_level"] = "fatal"
    expect($stdout).to receive(:print).exactly(2).times
    TxCatcher::LOGGER.report "example info message",  :info
    TxCatcher::LOGGER.report "example warn message",  :warn
    TxCatcher::LOGGER.report "example error message", :error
    TxCatcher::LOGGER.report "example fatal message", :fatal
    TxCatcher::Config["logger"]["stdout_level"] = "info"
  end

  it "converts Exception into a text for logging" do
    expect($stdout).to receive(:print).with("StandardError\n[no backtrace]\n")
    expect($stdout).to receive(:print).with("\n\n")
    TxCatcher::LOGGER.report StandardError.new, :error
  end

  it "prints additional data passed along" do
    expect($stdout).to receive(:print).with("example error message\n")
    expect($stdout).to receive(:print).with('  additional data: {:hello=>"world"}' + "\n")
    expect($stdout).to receive(:print).with("\n\n")
    TxCatcher::LOGGER.report "example error message", :error, data: { hello: "world" }
  end

end
