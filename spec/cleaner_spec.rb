require 'rubygems'
require 'ffi-rzmq'

require_relative 'spec_helper'
require_relative '../lib/txcatcher/models/address'
require_relative '../lib/txcatcher/models/transaction'
require_relative '../lib/txcatcher/cleaner'

RSpec.describe TxCatcher::Cleaner do

  before(:each) do
    allow(TxCatcher.rpc_node).to receive(:decoderawtransaction).and_return({ "vout" => []})
  end

  it "doesn't clean anything if transaction count is below threshold" do
    create_transactions(9)
    clean_transactions
    expect(TxCatcher::Transaction.count).to eq(9)
    expect(TxCatcher::Deposit.count).to     eq(9)
    expect(TxCatcher::Address.count).to     eq(9)
  end

  it "cleans excessive transactions from the DB" do
    create_transactions(15)
    clean_transactions
    expect(TxCatcher::Transaction.count).to eq(9)
    expect(TxCatcher::Deposit.count).to     eq(9)
    expect(TxCatcher::Address.count).to     eq(9)
  end

  it "doesn't delete the address upon cleanup if it has another deposit associated with it" do
    create_transactions(15)

    d = TxCatcher::Deposit.create(address_string: "addr1", amount: 0)
    tx = TxCatcher::Transaction.last
    tx.deposits << d
    tx.save

    clean_transactions
    expect(TxCatcher::Transaction.count).to eq(9)
    expect(TxCatcher::Deposit.count).to     eq(10)
    expect(TxCatcher::Address.count).to     eq(10)
  end

  it "protects checked transactions" do
    protected_txs = create_transactions(3, { protected: true })
    regular_txs   = create_transactions(15)
    clean_transactions
    expect(TxCatcher::Transaction.count).to eq(12)
    expect(TxCatcher::Deposit.count).to     eq(12)
    expect(TxCatcher::Address.count).to     eq(12)
  end


  def create_transactions(n, attrs={})
    (1..n).to_a.map do |i|
      d  = TxCatcher::Deposit.new(address_string: "addr#{i}", amount: 0)
      tx = TxCatcher::Transaction.new(attrs)
      tx.deposits << d
      tx.save
      tx
    end
  end

  def clean_transactions
    TxCatcher::Cleaner.start(run_once: true)
    sleep 1 until TxCatcher::Cleaner.stopped?
  end

end
