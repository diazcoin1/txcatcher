require 'rubygems'
require 'yaml'
require 'json'
require 'crypto-unit'
require 'sequel'
require 'sentry-raven'
Sequel.extension :migration

db_file = File.dirname(__FILE__) + "/config/txcatcher_test.db"
File.delete(db_file) if File.exists?(db_file)

require_relative '../lib/txcatcher/logger'
require_relative '../lib/txcatcher/utils/hash_string_to_sym_keys'
require_relative '../lib/txcatcher/utils/crypto_unit'
require_relative '../lib/txcatcher/config'
require_relative '../lib/txcatcher/initializer'
require_relative '../lib/txcatcher/bitcoin_rpc'

include TxCatcher::Initializer
ConfigFile.set!(File.dirname(__FILE__) + "/config/config.yml")

read_config_file
initialize_sentry
connect_to_db
run_migrations if migrations_pending?
connect_to_rpc_node

require_relative '../lib/txcatcher/models/transaction'
require_relative '../lib/txcatcher/models/address'
require_relative '../lib/txcatcher/models/deposit'
Sequel::Model.plugin :dirty

def unhexlify(msg)
  msg.scan(/../).collect { |c| c.to_i(16).chr }.join
end

RSpec.configure do |config|

  LOGFILE = File.expand_path(File.dirname(__FILE__) + "/config/txcatcher.log")
  ERRFILE = File.expand_path(File.dirname(__FILE__) + "/config/error.log")

  config.default_formatter = 'doc'
  config.before(:all) do
    #TxCatcher::LOGGER.reporters = [:logfile]
  end

  config.after(:all) do
  end

  config.before(:each) do
    TxCatcher::Transaction.select_all.delete
    TxCatcher::Deposit.select_all.delete
    TxCatcher::Address.select_all.delete
  end

  config.after(:each) do
    delete_log_files
  end

  def delete_log_files
    [LOGFILE, ERRFILE].each do |f|
      FileUtils.rm(f) if File.exist?(f)
    end
  end

end
