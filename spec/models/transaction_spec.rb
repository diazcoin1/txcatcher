require_relative '../spec_helper'

RSpec.describe TxCatcher::Transaction do

  class TxCatcher::Transaction
    def assign_transaction_attrs
      self.txid = @tx_hash["txid"] unless self.txid
    end
  end

  before(:each) do
    @hextx = File.read(File.dirname(__FILE__) + "/../fixtures/transaction.txt").strip
    @transaction = TxCatcher::Transaction.create(hex: @hextx)
  end

  it "parses rawtx using bitcoind" do
    expect(@transaction.txid).to eq("faf8368fec418a971e8fa94943e951490759498e0b2d3c4d5b9b12212c4f2e5a")
  end

  it "creates addresses and deposits" do
    expect(TxCatcher::Address.where(address: "323LGzCm43NgbtoYJhT6oKSwmKFTQ7AHzH").first.received).to eq(466700000)
    expect(TxCatcher::Address.where(address: "36u6xv2TvZqPPYdogzfLZpMAXrYdW4Vwjp").first.received).to eq(1332776478)
  end

  it "doesn't create duplicate deposits if valid? called manually before save" do
    TxCatcher::Transaction.select_all.delete
    transaction = TxCatcher::Transaction.new(hex: @hextx)
    transaction.valid?
    transaction.save
    expect(transaction.deposits.size).to eq(2)
    transaction.update(block_height: 1)
    expect(TxCatcher::Transaction.where(txid: transaction.txid).count).to eq(1)
  end

  it "updates block height by making manual requests to RPC and searching if tx is included in one of the previous blocks" do
    expect(TxCatcher.rpc_node).to receive(:getblockhash).exactly(10).times.and_return("blockhash123")
    expect(TxCatcher.rpc_node).to receive(:getblock).exactly(10).times.and_return({ "height" => "123", "tx" => [@transaction.txid], "hash" => "blockhash123"})
    @transaction.check_block_height!
    expect(@transaction.block_height).to eq(123)
  end

  it "updates multiple records at once" do
    transaction1 = TxCatcher::Transaction.create(hex: @hextx, txid: "123")
    transaction2 = TxCatcher::Transaction.create(hex: @hextx, txid: "1234")
    transaction3 = TxCatcher::Transaction.create(hex: @hextx, txid: "1235")
    transaction2.block_height = 2
    transaction3.block_height = 3
    expect(
      TxCatcher::Transaction.update_all([transaction1, transaction2, transaction3])
    ).to eq([transaction2.id, transaction3.id])
  end

end
