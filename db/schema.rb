Sequel.migration do
  change do
    create_table(:addresses) do
      String :address, :size=>255, :null=>false
      
      primary_key [:address]
    end
    
    create_table(:deposits, :ignore_index_errors=>true) do
      primary_key :id
      Bignum :amount, :null=>false
      Integer :transaction_id
      Integer :address_id
      
      index [:address_id]
      index [:transaction_id]
    end
    
    create_table(:schema_info) do
      Integer :version, :default=>0, :null=>false
    end
    
    create_table(:transactions) do
      String :txid, :size=>255, :null=>false
      DateTime :created_at
      TrueClass :protected, :default=>false
      
      primary_key [:txid]
    end
  end
end
