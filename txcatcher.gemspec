Gem::Specification.new do |s|
  s.name = "txcatcher".freeze
  s.version = "0.2.4"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Roman Snitko".freeze]
  s.date = "2018-10-02"
  s.description = "Ccurrently, the only job of this gem is to collect all new Bitcoin/Litecoin transactions, store them in a DB, index addresses.".freeze
  s.email = "roman.snitko@gmail.com".freeze
  s.executables = ["txcatcher".freeze, "txcatcher-monitor".freeze]
  s.extra_rdoc_files = [
    "LICENSE.txt",
    "README.md"
  ]
  s.files = [
    ".document",
    ".rspec",
    "Gemfile",
    "Gemfile.lock",
    "LICENSE.txt",
    "README.md",
    "Rakefile",
    "VERSION",
    "bin/txcatcher",
    "bin/txcatcher-monitor",
    "db/migrations/001_create_transactions.rb",
    "db/migrations/002_create_addresses.rb",
    "db/migrations/003_create_deposits.rb",
    "db/migrations/004_add_timestamps_to_transactions.rb",
    "db/migrations/005_add_protected_flag_to_transactions.rb",
    "db/schema.rb",
    "lib/tasks/db.rake",
    "lib/txcatcher.rb",
    "lib/txcatcher/bitcoin_rpc.rb",
    "lib/txcatcher/catcher.rb",
    "lib/txcatcher/cleaner.rb",
    "lib/txcatcher/config.rb",
    "lib/txcatcher/initializer.rb",
    "lib/txcatcher/logger.rb",
    "lib/txcatcher/models/address.rb",
    "lib/txcatcher/models/deposit.rb",
    "lib/txcatcher/models/transaction.rb",
    "lib/txcatcher/server.rb",
    "lib/txcatcher/utils/crypto_unit.rb",
    "lib/txcatcher/utils/hash_string_to_sym_keys.rb",
    "spec/catcher_spec.rb",
    "spec/cleaner_spec.rb",
    "spec/config/config.yml.sample",
    "spec/fixtures/transaction.txt",
    "spec/fixtures/transaction_decoded_no_outputs.txt",
    "spec/logger_spec.rb",
    "spec/models/transaction_spec.rb",
    "spec/spec_helper.rb",
    "spec/utils/crypto_unit.rb",
    "templates/config.yml",
    "txcatcher.gemspec"
  ]
  s.homepage = "http://github.com/snitko/txcatcher".freeze
  s.licenses = ["MIT".freeze]
  s.rubygems_version = "2.7.6".freeze
  s.summary = "An lightweight version of Bitpay's Insight, allows to check Bitcoin/Litecoin addresses".freeze

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<goliath>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<sequel>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<ffi-rzmq>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<crypto-unit>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<sentry-raven>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<aws-sdk-ses>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<faraday>.freeze, [">= 0"])
      s.add_development_dependency(%q<bundler>.freeze, [">= 0"])
      s.add_development_dependency(%q<jeweler>.freeze, [">= 0"])
    else
      s.add_dependency(%q<goliath>.freeze, [">= 0"])
      s.add_dependency(%q<sequel>.freeze, [">= 0"])
      s.add_dependency(%q<ffi-rzmq>.freeze, [">= 0"])
      s.add_dependency(%q<crypto-unit>.freeze, [">= 0"])
      s.add_dependency(%q<sentry-raven>.freeze, [">= 0"])
      s.add_dependency(%q<aws-sdk-ses>.freeze, [">= 0"])
      s.add_dependency(%q<faraday>.freeze, [">= 0"])
      s.add_dependency(%q<bundler>.freeze, [">= 0"])
      s.add_dependency(%q<jeweler>.freeze, [">= 0"])
    end
  else
    s.add_dependency(%q<goliath>.freeze, [">= 0"])
    s.add_dependency(%q<sequel>.freeze, [">= 0"])
    s.add_dependency(%q<ffi-rzmq>.freeze, [">= 0"])
    s.add_dependency(%q<crypto-unit>.freeze, [">= 0"])
    s.add_dependency(%q<sentry-raven>.freeze, [">= 0"])
    s.add_dependency(%q<aws-sdk-ses>.freeze, [">= 0"])
    s.add_dependency(%q<faraday>.freeze, [">= 0"])
    s.add_dependency(%q<bundler>.freeze, [">= 0"])
    s.add_dependency(%q<jeweler>.freeze, [">= 0"])
  end
end

