# Credits for this file go to
# Mikica Ivosevic https://github.com/mikicaivosevic/bitcoin-rpc-ruby/

require 'net/http'
require 'uri'
require 'json'

class BitcoinRPC

  def initialize(service_url)
    @uri = URI.parse(service_url)
  end

  def method_missing(name, *args)
    post_body = { 'method' => name, 'params' => args, 'id' => 'jsonrpc' }.to_json
    resp = JSON.parse( http_post_request(post_body) )
    raise JSONRPCError.new(resp['error'].merge({ "method" => name.to_s })) if resp['error']
    resp['result']
  end

  def http_post_request(post_body)
    http    = Net::HTTP.new(@uri.host, @uri.port)
    request = Net::HTTP::Post.new(@uri.request_uri)
    request.basic_auth @uri.user, @uri.password
    request.content_type = 'application/json'
    request.body = post_body
    http.request(request).body
  end

  def get_block_transactions(block_height)
    block_hash = self.getblockhash(block_height)
    TxCatcher.rpc_node.getblock(block_hash)
  end

  def get_blocks(limit=TxCatcher::Config[:max_blocks_in_memory])
    # We cache blocks we get from RPC to avoid repetetive requests
    # which are very slow.
    @blocks ||= {}

    blocks_removed = []
    @blocks.delete_if do |height,hash|
      if height < TxCatcher.current_block_height-TxCatcher::Config[:max_blocks_in_memory]
        blocks_removed << height
        true
      end
    end
    TxCatcher::LOGGER.report(
      "--- removing blocks\n#{blocks_removed.join(", ")}\nfrom cache, they're below the config " +
      "setting of #{TxCatcher::Config[:max_blocks_in_memory]}"
    ) unless blocks_removed.empty?

    blocks_cached = []
    limit.times do |i|
      height = TxCatcher.current_block_height - i
      unless @blocks[height]
        blocks_cached << height
        @blocks[height] = get_block_transactions(height)
      end
    end
    TxCatcher::LOGGER.report(
      "--- loading (from RPC) and caching transactions in blocks " +
      blocks_cached.join(", ")
    ) unless blocks_cached.empty?
    @blocks
  end

  class JSONRPCError < RuntimeError
    attr_accessor :data
    def initialize(data)
      super(data.to_s)
      self.data = data
    end
  end

end
