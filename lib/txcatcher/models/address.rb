module TxCatcher

  class Address < Sequel::Model
    one_to_many :deposits

    def received_in_btc
      CryptoUnit.new(Config["currency"], self.received, from_unit: :primary).to_standart
    end

  end


end
