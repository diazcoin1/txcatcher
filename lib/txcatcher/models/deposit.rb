module TxCatcher

  class Deposit < Sequel::Model
    many_to_one :transaction
    many_to_one :address

    attr_accessor :address_string

    def before_save
      if @address_string
        self.address = Address.find_or_create(address: @address_string)
      end

      self.address.update(received: self.address.received + self.amount)
    end

    def amount_in_btc
      CryptoUnit.new(Config["currency"], self.amount, from_unit: :primary).to_standart
    end

  end

end
