module TxCatcher

  class Transaction < Sequel::Model

    plugin :validation_helpers
    one_to_many :deposits

    # Updates only those transactions that have changed
    def self.update_all(transactions)
      transactions_to_update = transactions.select { |t| !t.column_changes.empty? }
      transactions_to_update.each(&:save)
      return transactions_to_update.map(&:id)
    end

    def before_validation
      return if !self.new? || !self.deposits.empty?
      parse_transaction
      assign_transaction_attrs
      @tx_hash["vout"].uniq { |out| out["n"] }.each do |out|
        amount = CryptoUnit.new(Config["currency"], out["value"], from_unit: :standart).to_i if out["value"]
        address = out["scriptPubKey"]["addresses"]&.first
        # Do not create a new deposit unless it actually makes sense to create one
        if address && amount && amount > 0
          self.deposits << Deposit.new(amount: amount, address_string: address)
        end
      end
    end

    def before_create
      self.created_at = Time.now
    end

    def after_create
      self.deposits.each do |d|
        d.transaction = self
        d.save
      end
    end

    def tx_hash
      @tx_hash || parse_transaction
    end

    def confirmations
      if self.block_height
        TxCatcher.current_block_height - self.block_height + 1
      else
        0
      end
    end

    # Queries rpc node to check whether the transaction has been included in any of the blocks,
    # but only if current block_height is nil.
    def check_block_height!(dont_save: false, blocks: nil)
      return self.block_height if self.block_height
      blocks = TxCatcher.rpc_node.get_blocks(blocks_to_check_for_inclusion_if_unconfirmed) unless blocks

      for block in blocks.values do
        if block["tx"] && block["tx"].include?(self.txid)
          LOGGER.report "tx #{self.txid} block height updated to #{block["height"]}"
          self.update(block_height: block["height"]) if !dont_save
          return block["height"].to_i
        else
          return nil
        end
      end
    end

    # This calculates the approximate number of blocks to check.
    # So, for example, if transaction is less than 10 minutes old,
    # there's probably no reason to try and check more than 2-3 blocks back.
    # However, to make absolute sure, we always bump up this number by 10 blocks.
    # Over larger periods of time, the avg block per minute value should even out, so
    # it's probably going to be fine either way.
    def blocks_to_check_for_inclusion_if_unconfirmed(limit=TxCatcher::Config[:max_blocks_in_memory])
      return false if self.block_height
      created_minutes_ago = ((self.created_at - Time.now).to_i/60)
      blocks_to_check = (created_minutes_ago / 10).abs + 10
      blocks_to_check = limit if blocks_to_check > limit
      blocks_to_check
    end

    private

      def parse_transaction
        @tx_hash = TxCatcher.rpc_node.decoderawtransaction(self.hex)
      end

      def assign_transaction_attrs
        self.txid = @tx_hash["txid"]
      end

      def validate
        super
        validates_unique :txid
        errors.add(:base, "No outputs for this transactions") if self.deposits.empty?
      end

  end

end
