require 'rubygems'
require 'yaml'
require 'json'
require 'thread'
require 'crypto-unit'
require 'sequel'
require 'sentry-raven'
Sequel.extension :migration

require_relative 'txcatcher/utils/crypto_unit'
require_relative 'txcatcher/utils/hash_string_to_sym_keys'
require_relative 'txcatcher/logger'
require_relative 'txcatcher/bitcoin_rpc'
require_relative 'txcatcher/config'
require_relative 'txcatcher/initializer'
require_relative 'txcatcher/catcher'
require_relative 'txcatcher/cleaner'

extend TxCatcher::Initializer
prepare

require_relative 'txcatcher/models/transaction'
require_relative 'txcatcher/models/address'
require_relative 'txcatcher/models/deposit'
Sequel::Model.plugin :dirty
